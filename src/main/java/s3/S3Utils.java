package s3;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import com.amazonaws.util.IOUtils;
//import com.odhsolutions.algorithms.config.CredentialConfig;
//import com.odhsolutions.algorithms.exceptions.CalculatorRuntimeException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class S3Utils implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(S3Utils.class);

    public static File downloadFileFromS3(CredentialConfig credentials, String s3Src, String localDest) throws CalculatorRuntimeException {
        LOGGER.info("Downloading file [ {} ] from S3 started", s3Src);
        AmazonS3 s3Client = createAwsS3Client(S3Utils.authenticate(credentials), credentials.getS3Endpoint());
        File localFile = new File(localDest);
        s3Client.getObject(new GetObjectRequest(credentials.getS3Bucket(), s3Src), localFile);
        if (!localFile.exists() || !localFile.canRead()) {
            Exception e=null;
            throw new CalculatorRuntimeException("Unable to download file from S3. (File not exist or can not be read)", e);
        }
        LOGGER.info("Downloading file [ {} ] from S3 finished", s3Src);
        return localFile;
    }

    public static void uploadFileToS3(CredentialConfig credentials, File srcFile, String dstFile) throws CalculatorRuntimeException {
        uploadFileToS3(getTransferManager(credentials), credentials.getS3Bucket(), srcFile, dstFile);
    }

    public static void uploadFileToS3(AWSCredentials awsCredentials, String s3Endpoint, String bucket, File srcFile, String dstFile) throws CalculatorRuntimeException {
        uploadFileToS3(buildTransferManager(awsCredentials, s3Endpoint), bucket, srcFile, dstFile);
    }

    public static void copyStreamToS3(CredentialConfig credentials, InputStream src, String dstFile) throws IOException, CalculatorRuntimeException {
        copyStreamToS3(authenticate(credentials), credentials.getS3Endpoint(), credentials.getS3Bucket(), src, dstFile);
    }

    public static void copyStreamToS3(AWSCredentials awsCredentials, String s3Endpoint, String bucket, InputStream src, String dstFile) throws IOException, CalculatorRuntimeException {
        TransferManager tm = buildTransferManager(awsCredentials, s3Endpoint);
        uploadStreamToS3(tm, bucket, src, dstFile);
    }

    private static AWSCredentials authenticate(CredentialConfig credentials) {
        return new BasicAWSCredentials(credentials.getS3Key(), credentials.getS3Secret());
    }

    private static TransferManager getTransferManager(CredentialConfig credentials) {
        return buildTransferManager(authenticate(credentials), credentials.getS3Endpoint());
    }

    private static AmazonS3Client createAwsS3Client(AWSCredentials credentials, String s3Endpoint) {
        AmazonS3Client s3Client = new AmazonS3Client(credentials);
        s3Client.setEndpoint(s3Endpoint);
        return s3Client;
    }

    private static TransferManager buildTransferManager(AWSCredentials credentials, String s3Endpoint) {
        TransferManager transferManager = new TransferManager(credentials);
        transferManager.getAmazonS3Client().setEndpoint(s3Endpoint);
        return transferManager;
    }

    private static void uploadStreamToS3(TransferManager tm, String bucket, InputStream src, String dstFile) throws IOException, CalculatorRuntimeException {
        byte[] bytes = IOUtils.toByteArray(src);
        byte[] md5 = DigestUtils.md5(bytes);
        String streamMD5 = new String(Base64.encodeBase64(md5));

        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentLength(bytes.length);
        objectMetadata.setSSEAlgorithm(ObjectMetadata.AES_256_SERVER_SIDE_ENCRYPTION);
        objectMetadata.setContentMD5(streamMD5);

        LOGGER.info("Uploading stream to s3 : [ {} ] started", dstFile);

        Upload upload = tm.upload(new PutObjectRequest(bucket, dstFile, new ByteArrayInputStream(bytes), objectMetadata));
        try {
            upload.waitForCompletion();
            LOGGER.info("Uploading stream to s3 : [ {} ] completed.", dstFile);
        } catch (Exception e) {
            throw new CalculatorRuntimeException("Unable to upload stream from S3.", e);
        }
        tm.shutdownNow();
    }

    private static void uploadFileToS3(TransferManager tm, String bucket, File srcFile, String dstFile) throws CalculatorRuntimeException {
        LOGGER.info("Uploading file : [ {} ] to s3 : [ {} ] started", srcFile.getAbsolutePath(), dstFile);
        PutObjectRequest request = new PutObjectRequest(bucket, dstFile, srcFile);

        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setSSEAlgorithm(ObjectMetadata.AES_256_SERVER_SIDE_ENCRYPTION);
        request.setMetadata(objectMetadata);

        Upload upload = tm.upload(request);
        try {
            upload.waitForCompletion();
            LOGGER.info("Uploading file : [ {} ] to s3 : [ {} ] completed.", srcFile.getAbsolutePath(), dstFile);
        } catch (Exception e) {
            throw new CalculatorRuntimeException("Unable to upload file from S3.", e);
        }
        tm.shutdownNow();
    }
}
